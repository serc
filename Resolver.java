/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright 2008 Ledermueller Achim
 *
 * serc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

import java.net.URL;
import java.util.Vector;

class Resolver {

	public Vector<Result> urlsSe1;
	public Vector<Result> urlsSe2;
	public Vector<URL> domains;
	public Vector<URL> files;
	private static String[] path;


	Resolver(){
		urlsSe1 = new Vector<Result>();
		urlsSe2 = new Vector<Result>();
		domains = new Vector<URL>();
		files = new Vector<URL>();
	}

	public void compare(ResultSet r1, ResultSet r2) {
		for(int i=0; i<r1.results.size(); i++) {
			for(int j=0; j<r1.results.get(i).urls.size(); j++) {
				for(int k=0; k<r2.results.size(); k++) {
					for(int l=0; l<r2.results.get(k).urls.size(); l++) {
						if(r1.results.get(i).urls.get(j).equals(r2.results.get(k).urls.get(l))) {
							if(r1.results.get(i).getRelevantUrl() == null) {
								//sets relevant Url
								r1.results.get(i).setRelevantUrl(r1.results.get(i).urls.get(j));
								r2.results.get(k).setRelevantUrl(r2.results.get(k).urls.get(l));

								urlsSe1.add(r1.results.get(i));
								urlsSe2.add(r2.results.get(k));
							}
						} else if( !Resolver.getFilename( r1.results.get(i).urls.get(j)).equals("") &&
							    Resolver.getFilename(r1.results.get(i).urls.get(j)).equals(Resolver.getFilename(r2.results.get(k).urls.get(l)))
							 ) 
						{
							files.add(r1.results.get(i).urls.get(j));
							files.add(r2.results.get(k).urls.get(l));

						} else if(r1.results.get(i).urls.get(j).getHost().equals(r2.results.get(k).urls.get(l).getHost())) {
							domains.add(r1.results.get(i).urls.get(j));
							domains.add(r2.results.get(k).urls.get(l));
						}
					}
				}
			}
		}
	}

	public static String getFilename(URL url) {
		if(url.getFile().charAt(url.getFile().length()-1) == '/')
			return "";

		path = url.getFile().split("/");
		if(path.length == 0)
			return "";

		return path[path.length-1];
	}

	public String urlsToString() {
		String str = "";
		//for(int i=0;i<urls.size();i++){
		//	str += urls.get(i).toString() + "\n";
		//}
		return str; 
	}
	public String domainsToString() {
		String str = "";
		for(int i=0;i<domains.size();i++){
			str += domains.get(i) + "\n";
		}
		return str; 
	}
	public String filesToString() {
		String str = "";
		for(int i=0;i<files.size();i++){
			str += files.get(i) + "\n";
		}
		return str; 
	}
}
