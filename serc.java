/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright 2008 Ledermueller Achim
 *
 * serc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*; 
import java.awt.event.*; 
import java.util.Vector;
import java.util.*;

public class serc extends JFrame {
	Fetcher[] f = new Fetcher[6]; 
	Parser[] p = new Parser[6];
	ResultSet rSet0, rSet1;
	Resolver r;

	JButton go = new JButton("Go!");
	JLabel sE1 = new JLabel( "Suchmaschine 1 ", JLabel.RIGHT);
	JLabel sE2 = new JLabel( "Suchmaschine 2 ", JLabel.RIGHT);
	JLabel query = new JLabel( "Query ", JLabel.RIGHT);
	JComboBox sET1 = new JComboBox(new String[]{"scholar.google.com", "scientificcommons.org", "oaister.org", "scirus.com", "E-LIS" });
	JComboBox sET2 = new JComboBox(new String[]{"scholar.google.com", "scientificcommons.org", "oaister.org", "scirus.com", "E-LIS" });
	//JTextField queryT = new JTextField("information retrieval yates practical");
	JTextField queryT = new JTextField("LOV domain");
	JComboBox number = new JComboBox(new String[]{"30","60", "90"});

	JTable urlTable, fileTable, domainTable;
	JLabel sameUri = new JLabel("Same URI", JLabel.CENTER);
	JLabel sameFile = new JLabel("Same File", JLabel.CENTER);
	JLabel sameDomain = new JLabel("Same Domain", JLabel.CENTER);
	JLabel proceeding = new JLabel(" - ", JLabel.CENTER);


	String[] columnNamesUrl={"URI","Rank"};
	String[] columnNamesFile={"File","Host"};
	String[] columnNamesDomain={"Host","Path"};

	AbstractTableModel urlTm, fileTm, domainTm; 

	public serc(){

		go.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.out.println("1: " + sET1.getSelectedIndex());
				System.out.println("2: " + sET2.getSelectedIndex());
				System.out.println("3: " + queryT.getText());
				go(sET1.getSelectedIndex(), sET2.getSelectedIndex(), queryT.getText(), Integer.parseInt(number.getSelectedItem().toString()));
			}

		});

		sE1.setLabelFor(sET1);
		sE2.setLabelFor(sET2);
		query.setLabelFor(queryT);

		Box vb = Box.createVerticalBox();
		Box hb = Box.createHorizontalBox();

		JPanel panel = new JPanel();
		GridLayout grid = new GridLayout(1,4);
		grid.setHgap(50);
		panel.setLayout(grid);
		panel.add(query);
		panel.add(queryT);
		panel.add(number);
		panel.add(go);

		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1,2));
		panel1.add(sE1);
		panel1.add(sET1);

		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1,2));
		panel2.add(sE2);
		panel2.add(sET2);

		hb.add(panel1);
		hb.add(Box.createHorizontalStrut(25));
		hb.add(panel2);

		urlTm = new AbstractTableModel() { 
			public String getColumnName(int col) {
				if(col<0||col>1)
					return"";
				return columnNamesUrl[col].toString(); 
			} 
			public boolean isCellEditable(int row , int col){
				return false;
			}
			public int getRowCount() { 
				if(r == null)
					return 0;
				return r.urlsSe1.size()*2;
			} 
			public int getColumnCount() { 
				return columnNamesUrl.length;
			} 
			public Object getValueAt(int row, int col) {
				switch(col){
					case 0: if(row%2 == 0)
							return r.urlsSe1.get(row/2).getRelevantUrl().getHost() + r.urlsSe1.get(row/2).getRelevantUrl().getFile(); 
						else
							return"";
					case 1: if(row%2 == 0)
							return rSet0.results.indexOf(r.urlsSe1.get(row/2));
						else
							return rSet1.results.indexOf(r.urlsSe2.get(row/2));
					default: return"";
				}
			} 
			public void setValueAt(Object value, int row, int col) {
			}
		} ;
		fileTm = new AbstractTableModel() { 
			public String getColumnName(int col) {
				if(col<0||col>1)
					return"";
				return columnNamesFile[col].toString(); 
			} 
			public boolean isCellEditable(int row , int col){
				return false;
			}
			public int getRowCount() { 
				if(r == null)
					return 0;
				return r.files.size();
			} 
			public int getColumnCount() { 
				return columnNamesFile.length;
			} 
			public Object getValueAt(int row, int col) {
				switch(col){
					case 0: if(row%2 == 0)
							return Resolver.getFilename(r.files.get(row));
						else
							return "";
					case 1: return r.files.get(row).getHost();
					default: return"";
				}
			} 
			public void setValueAt(Object value, int row, int col) {
			}
		} ;
		domainTm = new AbstractTableModel() { 
			public String getColumnName(int col) {
				if(col<0||col>1)
					return"";
				return columnNamesDomain[col].toString(); 
			} 
			public boolean isCellEditable(int row , int col){
				return false;
			}
			public int getRowCount() { 
				if(r == null)
					return 0;
				return r.domains.size();
			} 
			public int getColumnCount() { 
				return columnNamesDomain.length;
			} 
			public Object getValueAt(int row, int col) {
				switch(col){
					case 0: if(row%2 == 0)
							return r.domains.get(row).getHost();
						else
							return "";
					case 1: return r.domains.get(row).getPath() + "?" + r.domains.get(row).getQuery();
					default: return"";
				}
			} 
			public void setValueAt(Object value, int row, int col) {
			}
		} ;

		urlTable = new JTable(urlTm);
		fileTable = new JTable(fileTm);
		domainTable = new JTable(domainTm);
		urlTable.setPreferredScrollableViewportSize(new Dimension(500, 250));
		fileTable.setPreferredScrollableViewportSize(new Dimension(500, 250));
		domainTable.setPreferredScrollableViewportSize(new Dimension(500, 180));
		
		vb.add(Box.createVerticalStrut(20));
		vb.add(panel);
		vb.add(Box.createVerticalStrut(20));
		vb.add(hb);
		vb.add(Box.createVerticalStrut(20));
		vb.add(proceeding);
		vb.add(Box.createVerticalStrut(20));
		vb.add(sameUri);
		vb.add(new JScrollPane(urlTable));
		vb.add(Box.createVerticalStrut(20));
		vb.add(sameFile);
		vb.add(new JScrollPane(fileTable));
		vb.add(Box.createVerticalStrut(20));
		vb.add(sameDomain);
		vb.add(new JScrollPane(domainTable));
		vb.add(Box.createVerticalStrut(20));

		setContentPane(vb);
		pack();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void go(int sg0, int sg1, String query, int number) {
		System.out.println("Proceeding!");
		proceeding.setText("Proceeding!");
		proceeding.updateUI();
		proceeding.updateUI();

		if(sg0 == sg1) {
			System.out.println("Choose different SearchEngines!");
			proceeding.setText("Choose different SearchEngines!");
			proceeding.updateUI();
			proceeding.updateUI();
			return;
		}

		//if(Fetcher.searchEngines[sg0].equals("all") || Fetcher.searchEngines[sg1].equals("all")) {
		//	f[0] = new Fetcher(); p[0] = new Parser();
		//	f[1] = new Fetcher(); p[1] = new Parser();
		//	f[2] = new Fetcher(); p[2] = new Parser();
		//	f[3] = new Fetcher(); p[3] = new Parser();
		//	f[4] = new Fetcher(); p[4] = new Parser();
		//	f[5] = new Fetcher(); p[5] = new Parser();

		//	if(!Fetcher.searchEngines[sg0].equals("all")) {
		//		rSet0 = p[0].extractData(Fetcher.searchEngines[sg0], f[0].getPage(Fetcher.searchEngines[sg0], query, number, false));
		//		rSet1 = new ResultSet();

		//		for(int i=0; i<Fetcher.searchEngines.length; i++) {
		//			if(i == sg0 || Fetcher.searchEngines[i].equals("all") || i == 4) //4 == eprints.eclis ELIS, very slow
		//				continue;
		//			rSet1.addAll(p[i+1].extractData(Fetcher.searchEngines[i], f[i+1].getPage(Fetcher.searchEngines[i], query, number, false)));
		//		}
		//	}else {
		//		rSet0 = p[0].extractData(Fetcher.searchEngines[sg1], f[0].getPage(Fetcher.searchEngines[sg1], query, number, false));
		//		rSet1 = new ResultSet();

		//		for(int i=0; i<Fetcher.searchEngines.length; i++) {
		//			if(i == sg1 || Fetcher.searchEngines[i].equals("all") || i == 4) //4 == eprints.eclis ELIS, very slow
		//				continue;
		//			rSet1.addAll(p[i+1].extractData(Fetcher.searchEngines[i], f[i+1].getPage(Fetcher.searchEngines[i], query, number, false)));
		//		}
		//	}

		//} else {

			f[0] = new Fetcher();
			p[0] = new Parser();

			f[1] = new Fetcher();
			p[1] = new Parser();

			r = new Resolver();

			rSet0 = p[0].extractData(Fetcher.searchEngines[sg0], f[0].getPage(Fetcher.searchEngines[sg0], query, number, false));
			Result.counter = 0; // Reset rank-counter
			rSet1 = p[1].extractData(Fetcher.searchEngines[sg1], f[1].getPage(Fetcher.searchEngines[sg1], query, number, false));

			r.compare(rSet0, rSet1);

		//}

		System.out.println("Done!");
		proceeding.setText("Done!");
		proceeding.updateUI();

		urlTm.fireTableDataChanged();
		domainTm.fireTableDataChanged();
		fileTm.fireTableDataChanged();

	}

	public static void main(String[] args) {
		new serc();
	}
}
