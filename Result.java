/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright 2008 Ledermueller Achim
 *
 * serc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

import java.util.Vector;
import java.net.URL;

class Result {
	public Vector<URL> urls;
	public static int counter = 0;
	private int rank;

	//Resolver sets this var. 
	private URL relevantUrl;
	private URL relevantDomain;
	private URL relevantFile;

	Result(){
		urls = new Vector<URL>();
		counter++;
		rank = counter;
	}

	Result(URL url) {
		urls = new Vector<URL>();
		urls.add(url);
		counter++;
		rank = counter;
	}

	public void addUrl(URL url) {
		urls.add(url);
	}

	public String toString() {
		String str = "------Result------\n";
		for(int i=0; i<urls.size(); i++) {
			str += "Result.url: " + i +"\n";
			str += ">> " + urls.get(i).getHost() + "\n";
			str += ">> " + urls.get(i).getFile() + "\n";
			str += ">> " + urls.get(i).getQuery() + "\n";
			str += ">>\n";
		}
		return str;
	}

	public boolean isEmpty() {
		if(urls.size() == 0)
			return true;
		else
			return false;
	}

	public URL getRelevantUrl() {
		return relevantUrl;
	}
	public void setRelevantUrl(URL url) {
		relevantUrl = url;
	}

	public URL getRelevantDomain() {
		return relevantDomain;
	}
	public void setRelevantDomain(URL url) {
		relevantDomain = url;
	}

	public URL getRelevantFile() {
		return relevantFile;
	}
	public void setRelevantFile(URL url) {
		relevantFile = url;
	}
	public int getRank() {
		return rank;
	}
}
