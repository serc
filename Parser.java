/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright 2008 Ledermueller Achim
 *
 * serc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Vector;
import java.net.URL;

class Parser {
	
	private Pattern pattern;
	private ResultSet rSet = new ResultSet();
	private Result result = new Result();

	public ResultSet extractData(String searchEngine, String page) {
		if(searchEngine.equals(Fetcher.searchEngines[0]) || searchEngine.equals(Fetcher.searchEngines[2])) {
			pattern  = Pattern.compile("\"\\s*(http|ftp)s?://.*?\"");	
		} else if(searchEngine.equals(Fetcher.searchEngines[1])) {
			pattern  = Pattern.compile("<link>.*?</link>");	
		} else if(searchEngine.equals(Fetcher.searchEngines[3])) {
			page = page.replaceAll("%3A",":");
			page = page.replaceAll("%3B",";");
			page = page.replaceAll("%3D","=");
			page = page.replaceAll("%3F","?");
			page = page.replaceAll("%23","#");
			page = page.replaceAll("%22","\"");
			page = page.replaceAll("%25","%");
			page = page.replaceAll("%27","'");
			page = page.replaceAll("%2B","+");
			page = page.replaceAll("%26","&");
			page = page.replaceAll("%2F","/");
			page = page.replaceAll("%7E","~");
			pattern  = Pattern.compile("\\?src=web&url=(http|ftp)s?://.*?\"");	
		} else if(searchEngine.equals(Fetcher.searchEngines[4])) {
			pattern  = Pattern.compile("\"\\s*http://eprints\\.rclis\\.org/archive/.*?\"");	
		} else if(searchEngine.equals("sc")) {
			pattern  = Pattern.compile("\"\\s*(http)s?://.*?\"");	
		} else if(searchEngine.equals("elis")) {
			//there is also a alternative location - later
			pattern  = Pattern.compile("\"\\s*http://eprints\\.rclis\\.org/archive/.*?\\.pdf\"");	
		} else {
			pattern  = Pattern.compile("<link>.*?</link>");
		}

		Matcher matcher = pattern.matcher(page);

		try{
                  //first if-else-clause the while would be better
			while(matcher.find()) {
				if(searchEngine.equals(Fetcher.searchEngines[0])) {

                      if(
                              (matcher.group().indexOf("scholar?") == -1 && 
                               matcher.group().indexOf("http://google.com/search?") == -1 &&
                               matcher.group().indexOf("http://www.google.com/search?") == -1 &&
                               matcher.group().indexOf("http://images.google.com/images?") == -1 &&
                               matcher.group().indexOf("http://video.google.com/videosearch?") == -1 &&
                               matcher.group().indexOf("http://news.google.com/news?") == -1 &&
                               matcher.group().indexOf("http://maps.google.com/maps?") == -1 &&
                               matcher.group().indexOf("http://google.com/search?") == -1 &&
                               matcher.group().indexOf("http://www.google.com/webhp?") == -1 &&
                               matcher.group().indexOf("http://www.google.com/webhp?") == -1 &&
                               matcher.group().indexOf("worldcat.org") == -1) &&
                               !matcher.group().equals("http://www.google.com/intl/en/about.html")
                        ) 
                              {
                                      rSet.addResult(new Result(new URL(matcher.group().replace('"', ' ').trim())));
                              }
                        } else if(searchEngine.equals(Fetcher.searchEngines[1])){

                                String str = matcher.group().replaceAll("<link>","");
                                str = str.replaceAll("</link>","");
                                if(str.indexOf("opensearch") == -1) {
                                        Fetcher f = new Fetcher();
                                        Parser p = new Parser();
                                        str = f.getPage(str, "", 0, true);
                                        rSet.addAll(p.extractData("sc", str));

                                }
                        } else if(searchEngine.equals(Fetcher.searchEngines[2])) {

                                if(
                                                matcher.group().indexOf("quod.lib.umich.edu") == -1 && 
                                                matcher.group().indexOf("google-analytics.com") == -1 &&
                                                matcher.group().indexOf("www.oaister.org") == -1 &&
                                                matcher.group().indexOf("openarchives.org") == -1 &&
                                                matcher.group().indexOf("www.lib.umich.edu/opensearch/oaister.xml") == -1 &&
                                                matcher.group().equals("\"http://www.umdl.umich.edu/\"") == false &&
                                                matcher.group().equals("\"http://www.umich.edu/\"") == false
                                  ) 
                                {
                                        rSet.addResult(new Result(new URL(matcher.group().replace('"', ' ').trim())));
                                }
                        } else if(searchEngine.equals(Fetcher.searchEngines[3])) {
                                if(matcher.group().indexOf("mail.elsevier-alerts.com") == -1) {
                                        rSet.addResult(new Result(new URL(matcher.group().replace('"', ' ').trim().replaceAll("\\?src=web&url=", ""))));
                                }
                        } else if(searchEngine.equals(Fetcher.searchEngines[4])) {

                                String str = matcher.group().replace('"', ' ').trim();
                                Fetcher f = new Fetcher();
                                Parser p = new Parser();
                                str = f.getPage(str, "", 0, false);
                                rSet.addAll(p.extractData("elis", str));

                        } else if(searchEngine.equals("sc")) {
                                if(
                                                matcher.group().indexOf("scientificcommons.org") == -1 && 
                                                matcher.group().indexOf("www.w3.org") == -1 && 
                                                matcher.group().indexOf("google") == -1
                                  ) 
                                {

						result.addUrl(new URL(matcher.group().replace('"', ' ').trim()));
					}

				} else if(searchEngine.equals("elis")) {
						result.addUrl(new URL(matcher.group().replace('"', ' ').trim()));

				}
			}
			//if a result with more than one url was needed (scientificcommons)
			if(!result.isEmpty())
				rSet.addResult(result);

		}catch(Exception e) {
			System.err.println(e);
		}

		return rSet;
	}
}
