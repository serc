/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright 2008 Ledermueller Achim
 *
 * serc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/

import java.io.*;
import java.net.*;
import java.util.zip.GZIPInputStream;

class Fetcher {
	
	private URL url;
	private String host, filename;
	private int port, bytes_read;  
	private byte[] buffer; 
	private InputStream from_server;
	private StringBuffer output;
	private PrintWriter to_server;
	private boolean goOn;
	private int page;
	
	public static String[] searchEngines = 	{
		"http://scholar.google.com/scholar?q=",
		"http://www.scientificcommons.org/opensearch?search_string=",
		"http://quod.lib.umich.edu/cgi/b/bib/bib-idx?type=boolean&c=oaister&rgn1=entire+record&op2=and&rgn2=entire+record&sort=weighted+hit+frequency&submit2=search&sourceid=Mozilla-search&q1=",
		"http://www.scirus.com/srsapp/search?q=",
		"http://eprints.rclis.org/perl/search/simple?title_srchtype=ALL&_satisfyall=ALL&full_merge=ALL&_order=byyear&_action_search=Submit&full="
	};

	Fetcher() {
		goOn = true;
		page = 0;
		output = new StringBuffer();
	}

	//Returns the content of 'page'+'query' as String
	//gzip only for scientificcommon!!
	public String getPage(String page, String query, int number, boolean gzip) {
		try {
			while(goOn) {
				goOn = false;
				url = new URL(page);
				if(!url.getProtocol().equals("http"))
					throw new IllegalArgumentException("Protocol must be 'http:'");

				host = url.getHost();

				if((port = url.getPort()) == -1) port = 80;

                        //is required, because scientificcommons sends gziped even gzip;q=0 is set
				if((filename = url.getFile()).equals(""))
					filename = "/";

				filename += transformQuery(page, query, number);
				System.out.println("Filename: " + filename);

				Socket socket = new Socket(host, port);


				if(gzip)
					//Only for scientificcommons!!!!
					from_server = new GZIPInputStream(url.openStream());
				else
					from_server = socket.getInputStream();

				to_server = new PrintWriter(socket.getOutputStream());

				to_server.print("GET " + filename + " HTTP/1.1"+"\n");
				to_server.print("Host: " + host + "\n");
				to_server.print("User-Agent: my\n");
				to_server.print("Accept: text/plain,text/html\n");
				to_server.print("Accept-Language: en-us,en;q=0.5\n");
				to_server.print("Accept-Encoding: gzip;q=0,deflate;q=0,compress;q=0\n");
				to_server.print("Accept-Charset: ISO-8859-15\n");
				//to_server.print("Keep-Alive: 300\n");
				//to_server.print("Connection: keep-alive\n");
				to_server.print("\n");
				to_server.flush();

				buffer = new byte[4096];
				while((bytes_read = from_server.read(buffer)) != -1)
					output.append(new String(buffer, 0, bytes_read));

				socket.close();
			}
		}catch(Exception e) {
			System.err.println(e);
		}	
		//System.out.println(output.toString());
		return output.toString();
	}

	//Returns the query compatible to the in searchEngine specified Searchengine
	private String transformQuery(String searchEngine, String query, int number){
		//scholar.google.com
		if(searchEngine.equals(Fetcher.searchEngines[0])){
			query = query.replaceAll("\\s", "+");
			query += "&num=" + number; //max 100, if you need more use &num=100&start={0,100,200,300.....} and a the while(goOn)-loop
			//goOn = false; //set true if you need the while(goOn)-loop
		}
		//scientificcommons.org/opensearch
		else if(searchEngine.equals(Fetcher.searchEngines[1])) {
			query = query.replaceAll("\\s", "%20");

			query += "&start="+ (++page);

			//gets 2 pages.... == 30 results
			if(page < (number/15))
				goOn = true;
            //www.openaoister.com
		} else if(searchEngine.equals(Fetcher.searchEngines[2])) {
			query = query.replaceAll("\\s", "+");
			query += "&size=" + number; 
            //scirus.com
		} else if(searchEngine.equals(Fetcher.searchEngines[3])) {
			query = query.replaceAll("\\s", "+");
			query += "&p=" + page; 
			page += 10;

			if(page < (number))
				goOn = true;
            //eprints.rclis.org
		} else if(searchEngine.equals(Fetcher.searchEngines[4])) {
			query = query.replaceAll("\\s", "+");
			query += "&_offset=" + page; 
			page += 40;

			if(page < (number))
				goOn = true;
		}

		return query;
	}
}
